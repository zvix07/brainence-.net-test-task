﻿using System;
using System.Collections.Generic;

namespace ConsoleApp
{
    class Program
    {
        static  int[] array = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        static void Main(string[] args)
        {
            Console.WriteLine("Input your number");
            int user_number;
            try
            {
                user_number = Convert.ToInt32(Console.ReadLine());
            }
            catch 
            {
                Console.WriteLine("Error: Uncorrect number");
                return;
            }
            var result = FindElements(user_number);
            Console.WriteLine("Result:");
            foreach (var res in result)
                Console.WriteLine($"Elements with indexes {res.Item1} (valule:{array[res.Item1]}) and {res.Item2} (valule:{array[res.Item2]}) ");

        }

        static List<(int,int)> FindElements(int expected_sum)
        {
            List<(int, int)> result = new List<(int, int)>();
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = i; j < array.Length; j++)
                {
                    if (i != j && array[i] + array[j] == expected_sum)
                        result.Add( (i, j) );
                }
            }
            /// also using LINQ can give same result, but in my opinion using simple loop for this task is better and faster 
            return result;
        }
    }
}
