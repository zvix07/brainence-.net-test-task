﻿using System;
using System.Linq;

namespace ConsoleAppPalindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input your word:");
            string input_word = Console.ReadLine();
            if (IsPalindrome(input_word))
                Console.WriteLine($"Word \"{input_word}\" is palindrome");
            else
                Console.WriteLine($"Word \"{input_word}\" is NOT palindrome");

            Console.ReadKey();
        }

       static bool IsPalindrome( string word)
        {
            string revese_word = new string (word.Reverse().ToArray());

            return word.ToLower() == revese_word.ToLower();
        }
    }
}
