using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace WebApplication
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
           
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.Use(async (context, next) =>
            {
              if (  context.Request.Query.ContainsKey("TestTask") &&  context.Request.Query["TestTask"] == "ping" )
                context.Request.Headers.Add("TestTask", "ping");
                await next.Invoke();
            });
            app.Use( async (context, next) =>
            {
                if (context.Request.Headers.ContainsKey("TestTask") && context.Request.Headers["TestTask"]== "ping")
                {  
                    context.Response.Headers.Add("TestTask", "pong");
                }
               await next.Invoke();
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {

                    await context.Response.WriteAsync($"Request: \n");
                    foreach (var item in context.Request.Headers)
                    {
                        await context.Response.WriteAsync($"Name: {item.Key}  Value: {item.Value}   \n");
                    }
                    await context.Response.WriteAsync($"----------------------------- \n");
                    await context.Response.WriteAsync($"Responce: \n");
                    foreach (var item in context.Response.Headers)
                    {
                        await context.Response.WriteAsync($"Name: {item.Key}  Value: {item.Value}  \n ");
                    }
                });
             
            });
            
        }
    }
}
